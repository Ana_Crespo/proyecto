<?php

require_once("connect.php");

$user = json_decode(file_get_contents("php://input"),true); //recibe y decodifica json
$connection= connect();
$db= $connection->sportscenter;
$collection= $db->user;



$filter= array('email'=> $user['email'], 'password'=> $user['password']);
$login= $collection->findOne($filter);

if (!$login) {
    die("wronglogin");
}


//si es correcto, enviamos _id y name (y photo, si hay) para iniciar sesión

if (!isset($login['photo'])) {
    $send= (array('_id'=> $login['_id'], 'name'=> $login['name']));

} else {
    $send= (array('_id'=> $login['_id'], 'name'=> $login['name'], 'photo'=> $login['photo']));
}
echo json_encode($send);

disconnect($connection);

?>