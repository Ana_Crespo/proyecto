<?php

require_once("connect.php");

$connection= connect();
$db= $connection->sportscenter;
$collection= $db->sports;

$filter= array();
$options= array();
$result= $collection->find($filter, $options);

$sports= array();
foreach ($result as $doc)
{
    $sports[]= $doc;
}


disconnect($connection);
echo json_encode($sports);


?>