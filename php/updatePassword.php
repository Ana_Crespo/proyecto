<?php

require_once("connect.php");

$data= json_decode(file_get_contents('php://input'), true);
$id= $data['id'];
$oldPassword= $data['oldPassword'];
$newPassword= $data['newPassword'];

$connection= connect();
$db= $connection->sportscenter;
$collection= $db->user;

//comprobar que la contraseña (oldPassword) coincide con la del usuaio
$filter= array('_id'=> new MongoId($id));
$options= array('password'=> 1);
$result= $collection->findOne($filter, $options);
if ($result['password'] !== $oldPassword) {
    die("wrong");
}


//cambiar la contraseña
$update= array('password'=> $newPassword);
$result= $collection->update($filter, array('$set'=> $update));

disconnect($connection);

?>