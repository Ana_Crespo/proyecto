<?php

require_once("connect.php");

$data= json_decode(file_get_contents('php://input'), true);
$id= $data['id'];
$notifications= $data['notifications'];

$connection= connect();
$db= $connection->sportscenter;
$collection= $db->user;


$filter= array('_id'=> new MongoId($id));
$update= array('notifications'=> $notifications);
$result= $collection->update($filter, array('$set'=> $update));


disconnect($connection);

?>