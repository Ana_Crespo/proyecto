<?php

require_once("connect.php");

$province= $_REQUEST['province'];
$connection= connect();
$db= $connection->sportscenter;
$collection= $db->location;

$filter= array('province'=> $province);
$options= array('_id'=> 0, 'towns'=> 1);
$result= $collection->find($filter,$options);

$towns= array();
foreach ($result as $doc)
{
    $towns[]= $doc;
}

disconnect($connection);
echo json_encode($towns);


?>