<?php

function connect() {

    try {
        $connection= new Mongo("mongodb://localhost:27017");
        return $connection;
    } catch (MongoConnectionException $e) {
        die("No se ha podido conectar a la base de datos. " .$e->getMessage());
    }
}

function disconnect($connection) {
    $connection->close();
}

?>