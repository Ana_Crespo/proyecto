<?php

require_once("connect.php");

$id= $_REQUEST['id'];
$connection= connect();
$db= $connection->sportscenter;
$collection= $db->user;

$filter= array('_id'=> new MongoId($id));
$options= array('_id'=> 1,'name'=> 1, 'email'=> 1, 'notifications'=> 1, 'gender'=> 1, 'birth'=> 1, 'photo'=> 1);
$result= $collection->findOne($filter, $options);

$result['birth']= date('d-m-Y', $result['birth']->sec); //pasamos fecha a string para mostrar


disconnect($connection);
echo json_encode($result);


?>