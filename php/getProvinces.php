<?php

require_once("connect.php");

$connection= connect();
$db= $connection->sportscenter;
$collection= $db->location;

$filter= array();
$options= array('province' => 1);
$result= $collection->find($filter, $options);

$provinces= array();
foreach ($result as $doc)
{
    $provinces[]= $doc;
}


disconnect($connection);
echo json_encode($provinces);


?>