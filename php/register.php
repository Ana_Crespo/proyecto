<?php

require_once("connect.php");

$newUser = json_decode(file_get_contents("php://input"),true); //recibe y decodifica json
$connection= connect();
$db= $connection->sportscenter;
$collection= $db->user;


//comprobar nombre unico
$filter= array('name'=> $newUser['name']);
$unique_name= $collection->findOne($filter);

if ($unique_name) {
    die("name");
}

//comprobar email unico
$filter2= array('email'=> $newUser['email']);
$unique_email= $collection->findOne($filter2);

if ($unique_email) {
    die("email");
}

//si no hay errores, cambiamos el formato de la fecha (ISODate), ponemos foto de perfil por defecto y hacemos insert
$yyyymmdd= implode('-',array_reverse(explode('/',$newUser['birth'])));//lee la fecha al reves para devolver yyyy-mm-dd
$newUser['birth']= new MongoDate(strtotime($yyyymmdd));
//$newUser['photo']= "../images/profile/default.png";
$collection->insert($newUser);


disconnect($connection);

?>
