/**
 * Created by Yo on 11/05/16.
 */
"use strict";
function getAge(date){
    var now=new Date();
    var array_date = date.split("/");
    var age=parseInt(now.getFullYear())-parseInt(array_date[2])-1;

    //comprobar mes
    if (now.getMonth()-parseInt(array_date[1])+1 < 0) {
        return age;
    }
    if (now.getMonth()-parseInt(array_date[1])+1 > 0) {
        return age + 1;
    }
    //comprobar dia
    if (now.getDate()-parseInt(array_date[0]) >= 0) {
        return age + 1;
    }
    return age;
}

function checkChar(string) {
    var reg_exp= /^([A-Za-z]|[0-9]){3,}$/;
    return reg_exp.test(string);
}

function checkEmail(string) {
    var reg_exp= /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/;
    return reg_exp.test(string);
}