
angular.module('app.settings', ['ui.router', 'ngSanitize', 'ui.select'])

    .controller('settingsCtrl', ['$scope', '$http', function settingsCtrl($scope, $http) {

        $scope.mode= "personal";

        //cargamos datos de usuario
        $http.get("../php/getUser.php/?id=" +$scope.user._id.$id)
            .then(function (res) {
                $scope.user = res.data;
            });

        //cargamos todas las provincias
        $http.get("../php/getProvinces.php")
            .then(function (res) {
                $scope.provinces = res.data;
            });

        //cargamos todos los deportes
        $http.get("../php/getSports.php")
            .then(function (res) {
                $scope.sports = res.data;
            });

        //si cambia provincia, cargar localidades
        $scope.$watch("preferences.province",function(newValue) {
            if (newValue== null) {
                $scope.preferences.town= null; //si borra provincia, borra localidad
            } else {
                $http.get("../php/getTowns.php?province=" + $scope.preferences.province)
                    .then(function (res) {
                        $scope.towns = res.data[0].towns;
                    });
            }
        });


        $scope.$watch("mode",function(newValue) {
            if (newValue== "personal") {
                $scope.personal= {
                    "personalOk": false
                    };
                $http.get("../php/getPersonal.php/?id=" +$scope.user._id.$id)
                    .then(function (res) {
                        var personal = res.data;
                        $scope.personal = $.extend(true, {}, personal); //clonar datos
                    });
            } else if (newValue== 'password') {
                resetPasswordErrors();
                $scope.password = {
                    "oldPassword": "",
                    "newPassword": "",
                    "repeat": "",
                    "passwordOk": false
                };
            } else {
                $scope.preferences= {
                    "preferencesOk": false
                };

                //cargamos datos del usuario
                $http.get("../php/getPreferences.php/?id=" +$scope.user._id.$id)
                    .then(function (res) {
                        var preferences = res.data;
                        $scope.preferences = $.extend(true, {}, preferences); //clonar datos
                    });
            }
        });


        $scope.changePersonal= function() {
            $scope.personal.personalOk= false;
            var toSend= {"id": $scope.user._id.$id, "notifications": $scope.personal.notifications};
            $http.put("../php/updatePersonal.php", toSend)
                .then(function (res) {
                    $scope.personal.personalOk= true;
                });
        };

        function resetPasswordErrors() {
            $scope.wrongOldPassword= "";
            $scope.wrongNewPassword= "";
            $scope.wrongRepeat= "";
        }
        $scope.changePassword= function() {
            resetPasswordErrors();
            $scope.password.passwordOk= false;
            if ($scope.password.oldPassword.length < 6) {
                $scope.wrongOldPassword= "Contraseña incorrecta";
                return false;
            }
            if ($scope.password.newPassword.length < 6) {
                //comprobar password >= 6
                $scope.wrongNewPassword= "La contraseña debe contener al menos 6 caracteres alfa-numéricos";
            } else if (!checkChar($scope.password.newPassword)) {
                //comprobar que no hay caracteres especiales ni espacios en password
                $scope.wrongNewPassword= "La contraseña solo puede contener caracteres alfa-numéricos";
            } else if ($scope.password.newPassword !== $scope.password.repeat) {
                //comprobar que la contraseña repetida coincide con la introducida
                $scope.wrongRepeat= "Contraseña incorrecta";
            } else {
                var toSend= {"id": $scope.user._id.$id, "oldPassword": $scope.password.oldPassword, "newPassword": $scope.password.newPassword};
                $http.put("../php/updatePassword.php", toSend)
                    .then(function (res) {
                        if (res.data== "wrong") {
                            $scope.wrongOldPassword= "Contraseña incorrecta";
                        } else {
                            $scope.password = {
                                "oldPassword": "",
                                "newPassword": "",
                                "repeat": "",
                                "passwordOk": true
                            }
                        }
                    });
            }

        };



    }])