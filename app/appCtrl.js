angular
    .module("app", ["ui.router", "app.settings"])

    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('inicio', {
                url: "/",
                templateUrl: "dashboard/dashboard.html",
                controller: "dashboardCtrl"
            })
            .state('eventos', {
                url: "/eventos",
                templateUrl: "events/events.html",
                controller: "eventsCtrl"
            })
            .state('mensajes', {
                url: "/mensajes",
                templateUrl: "messages/messages.html",
                controller: "messagesCtrl"
            })
            .state('amigos', {
                url: "/amigos",
                templateUrl: "friends/friends.html",
                controller: "friendsCtrl"
            })
            .state('configuracion', {
                url: "/configuracion",
                templateUrl: "settings/settings.html",
                controller: "settingsCtrl"
            })
    })
    .controller('appCtrl', ['$scope', '$http', function indexCtrl($scope, $http) {

        $scope.user= JSON.parse(sessionStorage.getItem('user'));

        $scope.$watch(function () {
            return sessionStorage.getItem('user');
        }, function (newVal) {
            if (newVal==null) {
                location.href="../index.html";
            }
        }, true);

        $scope.logout= function() {
            sessionStorage.removeItem("user");
        };

    }])