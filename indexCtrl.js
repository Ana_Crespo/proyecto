/**
 * Created by Yo on 3/05/16.
 */
angular
    .module("app", [])

    .controller('indexCtrl', ['$scope', '$http', function indexCtrl($scope, $http) {

        $scope.register= null;
        $scope.newUser= null;
        $scope.$watch(sessionStorage['user'],function(newValue) {
            if (newValue != null) {
                location.href = "app/app.html";
            }
        });


        function resetErrors() {
            $scope.wrongName= "";
            $scope.wrongPassword= "";
            $scope.wrongRepeat= "";
            $scope.wrongEmail= "";
            $scope.wrongBirth= "";
        }
        function hasErrors() {
            if ($scope.wrongName!="" || $scope.wrongPassword!="" || $scope.wrongRepeat!="" || $scope.wrongEmail!="" || $scope.wrongBirth!="") {
                return true;
            }
            return false;
        }

        $scope.toggleRegister= function() {

            if ($scope.register == null) { //carga las provincias solo la primera vez que pulsa en registrar
                $http.get("php/getProvinces.php")
                    .then(function (res) {
                        $scope.provinces = res.data;
                    });
            }

            $scope.register= !$scope.register;

            if ($scope.register) { //si va a pantalla de registro, reinicia los valores
                $scope.repeat= "";
                $scope.newUser= {
                    "name": "",
                    "password": "",
                    "email": "",
                    "notifications": false,
                    "gender": "Male",
                    "birth": "",
                    "province": '',
                    "town": null
                };
            } else {
                $scope.wrongLogin= "";
                $scope.user= {
                    "email": "",
                    "password": ""
                }
            }

        };


        $scope.$watch("newUser.province",function(newValue,oldValue) {
            if (newValue==oldValue || $scope.newUser.province=="") {
                return;
            }

            if (newValue==null) { //si borra la provincia
                $scope.newUser.town= null;
            } else {//si selecciona una
                $http.get("php/getTowns.php?province=" + $scope.newUser.province)
                    .then(function (res) {
                        $scope.towns = res.data[0].towns;
                    });
            }
        });

        $scope.loginUser= function() {
            $scope.wrongLogin= "";
            $http.post("php/login.php", $scope.user)
                .then(function (res) {
                    if (res.data== "wronglogin") {
                        $scope.wrongLogin= "Usuario y/o contraseña incorrectos";
                    } else {
                        sessionStorage['user']= JSON.stringify(res.data);
                        location.href = "app/app.html";
                    }
                })
        };

        $scope.sendRegistration= function() {
            //borrar errores
            resetErrors();

            //comprobar que no hay caracteres especiales ni espacios en name
            if (!checkChar($scope.newUser.name)) {
                $scope.wrongName= "El nombre de usuario debe contener al menos 3 caracteres (letras y/o números)";
            }

            if ($scope.newUser.password.length < 6) {
                //comprobar password >= 6
                $scope.wrongPassword= "La contraseña debe contener al menos 6 caracteres alfa-numéricos";
            } else if (!checkChar($scope.newUser.password)) {
                //comprobar que no hay caracteres especiales ni espacios en password
                $scope.wrongPassword= "La contraseña solo puede contener caracteres alfa-numéricos";
            } else if ($scope.newUser.password !== $scope.repeat) {
                //comprobar que la contraseña repetida coincide con la introducida
                $scope.wrongRepeat= "Contraseña incorrecta";
            }

            //comprobar formato email
            if (!checkEmail($scope.newUser.email)) {
                $scope.wrongEmail= "Email incorrecto";
            }

            //comprobar birth > 14 años
            if (getAge($scope.newUser.birth) < 14) {
                $scope.wrongBirth= "Debes tener al menos 14 años";
            }

            if (!hasErrors()) {
                $http.post("php/register.php",$scope.newUser)
                    .then(function (res) {
                        if (res.data=='name') {
                            //nombre repetido
                            $scope.wrongName= "El nombre ya ha sido registrado";
                        } else if (res.data=='email') {
                            //email repetido
                            $scope.wrongEmail= "El email ya ha sido registrado";
                        } else {
                            swal("¡Bienvenido a SPORTSCENTER!", "Te has registrado correctamente, así que tira para el login", "success")
                        }
                    });
            }
        };

        $scope.remember= function() {
            swal({
                title: "",
                text: "Introduce tu email",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: ""
                },
                function(inputValue) {
                    if (inputValue === false) return false;
                    if (inputValue === "" || !checkEmail(inputValue)) {
                        swal.showInputError("Debes introducir el email con el que te registraste");
                        return false
                    }
                    $http.post("php/rememberPassword.php", inputValue)
                        .then(function (res) {
                            if (res.data== 'wrongmail') {
                                swal.showInputError("Email no registrado");
                            } else {
                                swal("", "Tu contraseña ha sido enviada a " +inputValue+ ". Consulta tu correo", "success");
                            }
                        });
                });

        };

    }])